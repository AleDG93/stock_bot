import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

import java.io.*;
import java.util.Properties;

import static java.lang.Thread.sleep;

public class main {

    public static void main(String[] args) throws IOException {

        Properties props = new Properties();
        props.put("bootstrap.servers", "localhost:9092");
        props.put("key.serializer", StringSerializer.class.getName());
        props.put("value.serializer", StringSerializer.class.getName());
        props.put("key.converter", "org.apache.kafka.connect.storage.StringConverter");
        props.put("value.converter", "org.apache.kafka.connect.storage.StringConverter");


        KafkaProducer<String, String> producer = new KafkaProducer<String, String>(props);


        int i = 1;

        while(true){
            try {
                String key = String.valueOf(System.currentTimeMillis());
                String rec = RequestHandler.get("https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol=MSFT&interval=1min&apikey=F4YUEH2AWNA9HG0Y");
                //String rec = "record number: " + i;
                ProducerRecord<String, String> record =
                        new ProducerRecord<String, String>("stock_options", key, rec); // (1)
                producer.send(record);
                System.out.println(rec);
                i += 1;
                sleep(60000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
