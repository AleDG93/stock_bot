#!/bin/bash
SPARK_APPLICATION_JAR_LOCATION="/opt/spark-apps/spark-1.0-SNAPSHOT.jar"
SPARK_APPLICATION_MAIN_CLASS="StockPredictor"
#SPARK_SUBMIT_ARGS="--conf spark.executor.extraJavaOptions='-Dconfig-path=/opt/spark-apps/dev/config.conf'"
#SPARK_SUBMIT_ARGS="--conf spark.executor.memory=2g"
#SPARK_SUBMIT_ARGS="--driver-memory 2g --executor-memory 1g"
docker run --network docker-spark-cluster-master_spark-network -v /mnt/spark-apps:/opt/spark-apps --env SPARK_APPLICATION_JAR_LOCATION=$SPARK_APPLICATION_JAR_LOCATION --env SPARK_APPLICATION_MAIN_CLASS=$SPARK_APPLICATION_MAIN_CLASS spark-submit:2.3.1
